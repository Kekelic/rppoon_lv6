﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>()
            {
                new Product("Bread",5),
                new Product("Milk",3.99),
                new Product("Meat",29.99)
            };

            Box box = new Box(products);
            IAbstractIterator iterator = box.GetIterator();

            for (int i = 0; i < box.Count; i++)
            {
                if (i == 0)
                {
                    Product product = iterator.First();
                    Console.WriteLine(product.ToString());
                }
                else
                {
                    Product product = iterator.Next();
                    Console.WriteLine(product.ToString());
                }
            }
        }
    }
}
