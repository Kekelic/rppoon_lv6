﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class StringDigitChecker : StringChecker
    {

        protected override bool PerformCheck(string stringToCheck)
        {
            foreach(char c in stringToCheck)
            {
                if (char.IsDigit(c))
                    return true;
            }
            return false;
        }

    }
}
