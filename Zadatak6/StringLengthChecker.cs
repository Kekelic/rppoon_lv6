﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class StringLengthChecker : StringChecker
    {
        private int MinLength { get; set; }

        public StringLengthChecker(int minlength)
        {
            this.MinLength = minlength;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= MinLength;

        }

    }
}
