﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLengthChecker lengthChecker = new StringLengthChecker(5);
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

            digitChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);

            if (digitChecker.Check("CheckString99"))
            {
                Console.WriteLine("String is good");
            }
            else
            {
                Console.WriteLine("String is bad");
            }

            if (digitChecker.Check("BadString"))
            {
                Console.WriteLine("String is good");
            }
            else
            {
                Console.WriteLine("String is bad");
            }

        }
    }
}
