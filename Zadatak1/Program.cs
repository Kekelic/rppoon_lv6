﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Note> notes = new List<Note>()
            {
                new Note("Buy", "bread and milk"),
                new Note("Do your homework", "rppon"),
                new Note("Training", "football at 8 p.m.")
            };

            Notebook notebook = new Notebook(notes);
            IAbstractIterator iterator = notebook.GetIterator();

            for (int i = 0; i < notebook.Count; i++)
            {
                if (i == 0)
                {
                    Note note = iterator.First();
                    note.Show();
                }
                else
                {
                    Note note = iterator.Next();
                    note.Show();
                }
            }

        }
    }
}
