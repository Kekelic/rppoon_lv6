﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
                new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);

            logger.Log("Login error", MessageType.ERROR);
            logger.Log("Login information", MessageType.INFO);
            logger.Log("Login warning", MessageType.WARNING);

        }
    }
}
