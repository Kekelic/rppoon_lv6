﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("Homework", "do your homework",new DateTime(2020, 5, 15));
            Console.WriteLine(toDoItem.ToString());
            careTaker.AddState(toDoItem.StoreState());

            toDoItem.ChangeTask("help a friend with homework");
            Console.WriteLine(toDoItem.ToString());

            toDoItem.RestoreState(careTaker.GetLastState());
            Console.WriteLine(toDoItem.ToString());

        }
    }
}
