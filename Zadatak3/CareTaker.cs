﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class CareTaker
    {
        public List<Memento> states;

        public CareTaker() { this.states = new List<Memento>(); }

        public void AddState(Memento memento) { this.states.Add(memento); }
        public void RemoveState(Memento memento) { this.states.Remove(memento); }
        public Memento GetLastState()
        {
            return this.states.Last();
        }
    }
}
