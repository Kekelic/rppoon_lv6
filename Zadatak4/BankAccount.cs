﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class BankAccount
    {
        private string ownerName;
        private string ownerAddress;

        private decimal balance;

        public BankAccount(string ownerName, string ownerAdderss, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAddress = ownerAdderss;
            this.balance = balance;
        }

        public void ChangeOwnerAdress(string adderss)
        {
            this.ownerAddress = adderss;
        }
        public void UpdateBalance(decimal amount) { this.balance += amount; }
        public string OwnerName { get { return this.ownerName; } }
        public string OwneerAddress { get { return this.ownerAddress; } }
        public decimal Balance { get { return this.balance; } }

        public override string ToString()
        {
            return this.ownerName + "\nadderss: " + this.ownerAddress + "\nbalance: " + this.balance;
        }

        public Memento StoreState()
        {
            return new Memento(this.ownerName, this.ownerAddress, this.balance);
        }

        public void RestoreState(Memento previous)
        {
            this.ownerName = previous.OwnerName;
            this.ownerAddress = previous.OwnerAddress;
            this.balance = previous.Balance;
        }

    }
}
