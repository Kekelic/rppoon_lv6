﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            //CareTaker careTaker = new CareTaker();
            BankAccount bankAccount = new BankAccount("Stjepan Kekelić", "Kneževac 18", 1000);
            Console.WriteLine(bankAccount.ToString());
            Memento memento = bankAccount.StoreState();

            bankAccount.ChangeOwnerAdress("Osijek 324");
            bankAccount.UpdateBalance(500);
            Console.WriteLine(bankAccount.ToString());

            bankAccount.RestoreState(memento);
            Console.WriteLine(bankAccount.ToString());
        }
    }
}
