﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class PasswordValidator
    {
        private StringChecker firstChecker;
        private StringChecker lastChecker;

        public PasswordValidator(StringChecker firstChecker)
        {
            this.firstChecker = firstChecker;
            this.lastChecker = firstChecker;
        }

        public void AddCheck(StringChecker stringChecker)
        {
            lastChecker.SetNext(stringChecker);
            lastChecker = stringChecker;
        }

        public bool Check(string password)
        {
            return firstChecker.Check(password);
        }
    }
}
