﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLengthChecker lengthChecker = new StringLengthChecker(5);
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

            PasswordValidator passwordValidator = new PasswordValidator(digitChecker);
            passwordValidator.AddCheck(lengthChecker);
            passwordValidator.AddCheck(upperCaseChecker);
            passwordValidator.AddCheck(lowerCaseChecker);

            if (passwordValidator.Check("123Rppoon"))
            {
                Console.WriteLine("Password is good");
            }
            else
            {
                Console.WriteLine("Password is bad");
            }

            if (digitChecker.Check("123rppoon"))
            {
                Console.WriteLine("Password is good");
            }
            else
            {
                Console.WriteLine("Password is bad");
            }

        }
    }
}
